#ifndef IO_H
#define IO_H

#include "console.h"
#include "consoleoutputstream.h"
#include "fileinputstream.h"
#include "fileoutputstream.h"
#include "inputstream.h"
#include "outputstream.h"
#include "stringinputstream.h"
#include "stringoutputstream.h"

#endif
